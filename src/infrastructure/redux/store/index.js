import { applyMiddleware, combineReducers, createStore } from "redux"
import thunk from "redux-thunk"
import { composeWithDevTools } from "redux-devtools-extension"
import gameReducer from "./reducers/gameReducer";

const reducers = combineReducers({
	gameReducer: gameReducer,
})

// on compose notre redux dev tool
const composedEnhancer = composeWithDevTools(
	// EXAMPLE: Add whatever middleware you actually want to use here
	applyMiddleware(thunk)
)

const storeReact = createStore(reducers, composedEnhancer)

export default storeReact
