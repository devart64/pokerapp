const rangs = [
	{lettre:"2",valeur:"2",},
	{lettre:"3",valeur:"3",},
	{lettre:"4",valeur:"4",},
	{lettre:"5",valeur:"5",},
	{lettre:"6",valeur:"6",},
	{lettre:"7",valeur:"7",},
	{lettre:"8",valeur:"8",},
	{lettre:"9",valeur:"9",},
	{lettre:"10",valeur:"T",},
	{lettre:"V",valeur:"J",},
	{lettre:"D",valeur:"Q",},
	{lettre:"R",valeur:"K",},
	{lettre:"A",valeur:"A",},
];

export default rangs;