import { actionsGameReducer } from "../redux/store/reducers/gameReducer";
import { connect } from "react-redux";
import Carte from "./carte";
import React from "react";

const Tableau = (props) => {

	return (
		<div className="col ">
			<div className="card">
				<div className={"card-header text-center"}><h2>Le tableau</h2></div>
				<div className="card-body">
					{props.gameReducer.cartesTableau && props.gameReducer.cartesTableau.map((carte, index) => {
						return <Carte key={index} carte={carte}/>
					})}
				</div>
				<div className={"card-footer"}></div>
			</div>
		</div>
	)
}

const mapStateToProps = (props) => props
const mapDispatchToProps = {
	...actionsGameReducer
}
export default connect(
	mapStateToProps,
	mapDispatchToProps,
)(Tableau)