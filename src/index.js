import React from 'react';
import ReactDOM from 'react-dom/client';
import './infrastructure/index.css';
import App from './infrastructure/App';
import reportWebVitals from './infrastructure/reportWebVitals';

import { Provider } from 'react-redux'

import store from "./infrastructure/redux/store"
const root = ReactDOM.createRoot(document.getElementById('root'));

console.log(process.env);
console.log(process.env.NODE_ENV);

if(process.env.NODE_ENV === 'production'){
    console.log('App running in production mode');
}

root.render(
    <Provider store={store}>
        <App />
    </Provider>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
