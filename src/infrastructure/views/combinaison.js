import { actionsGameReducer } from "../redux/store/reducers/gameReducer";
import { connect } from "react-redux";

const Combinaison = (props) => {

	return (
		<div>
			<p>{props.joueur && props.joueur.combinaison !== '' ? props.joueur.detailCombinaison.descr : 'Main : rien!'}</p>
		</div>
	)
}

const mapStateToProps = (props) => props
const mapDispatchToProps = {
	...actionsGameReducer,
}
export default connect(
	mapStateToProps,
	mapDispatchToProps,
)(Combinaison)
