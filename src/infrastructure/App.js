import "./App.css";
import React, { useEffect, useState } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import Carte from "./views/carte";
import Joueur from "./views/joueur";
import { connect, useSelector } from "react-redux";
import { actionsGameReducer } from "./redux/store/reducers/gameReducer";
import Tableau from "./views/tableau";
import joueur from "./views/joueur";

function App(props) {
	const [jeux, setjeux] = useState([]);

	// écouteur de l'état du type de liste dans le store redux
	const jeuRedux = useSelector((state) => state.gameReducer.cartes)
	const joueur = useSelector((state) => state.gameReducer.joueur)
	const ia = useSelector((state) => state.gameReducer.ia)
	const mainJoueurRedux = useSelector((state) => state.gameReducer.joueur.main)
	const mainJoueurIARedux = useSelector((state) => state.gameReducer.ia.main)


	useEffect(() => {
		props.addJoueur('Stephen')
	}, []);


	useEffect(() => {
		if (jeuRedux !== jeux) {
			setjeux(jeuRedux)
		}
	}, [jeuRedux]);


	const generationJeux = () => {
		props.genererCartes()
	};

	function melangerJeux() {
		props.melangerCartes();
	}

	const distribuerJeux = () => {
		props.distribuerCartes()
	}

	const tirerLaTurn = () => {
		props.tirerLaTurn()
	}

	const tirerLaRiver = () => {
		props.tirerLaRiver()
	}

	return (
		<div className="App">
			<div>
				<h1>Poker game</h1>
				<div style={{display: "block", width: "100%"}}>
					<div className="container">
						{jeux.map((carte, index) => {
							return (
								<Carte key={index} carte={carte}/>
							);
						})}
					</div>

                  <div className="container">
                    <div className="row align-items-center">
						<Tableau />
                    </div>
                  </div>

					<div className="container">
						<div className="row align-items-center">
							<Joueur joueur={joueur}/>
							<Joueur joueur={ia}/>
						</div>
					</div>
					{props.gameReducer.winner &&
						<div className="container">
							<div className="row align-items-center">
								<h2>La gagnant est : {props.gameReducer.winner} !</h2>
							</div>
						</div>
					}
					<div style={{marginTop: "250px"}}>
						<div>
							<button
								className="btn btn-primary m-2"
								onClick={() => generationJeux()}
							>
								Générer
							</button>
							<button
								className="btn btn-success m-2"
								onClick={() => melangerJeux()}
							>
								mélanger
							</button>
							<button
								className="btn btn-success m-2"
								onClick={() => distribuerJeux()}
							>
								distribuer
							</button>
							<button
								className="btn btn-success m-2"
								onClick={() => tirerLaTurn()}
							>
								turn
							</button>
							<button
								className="btn btn-success m-2"
								onClick={() => tirerLaRiver()}
							>
								river
							</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	);
}

const mapStateToProps = (props) => props
const mapDispatchToProps = {
	...actionsGameReducer
}
export default connect(
	mapStateToProps,
	mapDispatchToProps,
)(App)
