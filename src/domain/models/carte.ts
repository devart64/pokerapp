import { Couleur } from './couleur';
import { Figure } from './figure';
import { Valeur } from './valeur';
 export interface Carte  {
    valeur: Valeur,
    figure: Figure,
    couleur: Couleur
}
