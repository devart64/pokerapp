import React, { useState } from "react";
import Carte from "./carte";
import Combinaison from "./combinaison";

const Joueur = (props) => {

    return (
        <div className="col ">
            <div className="card">
                <div className={"card-header text-center"}>{props.joueur.nom}</div>
                <div className="card-body">
                    {props.joueur.main && props.joueur.main.map((carte, index) => {
                        return <Carte key={index} carte={carte} />
                    })
                    }
                </div>
                <div className={"card-footer"}>
                    <Combinaison joueur={props.joueur} />
                </div>
            </div>
        </div>
    )
}

export default Joueur