import couleurs from '../../../config/couleur'
import rangs from '../../../config/rang'
import { GiClubs, GiDiamonds, GiHearts, GiSpades } from "react-icons/gi";
import { IconContext } from "react-icons";
import React from "react";
var hand = require('pokersolver').Hand;
const initialState = {
	cartes: [],
	cartesTableau: [],
	cartesBrulees: [],
	joueur: {
		nom: '',
		main: [],
		combinaison: [],
		detailCombinaison: {}
	},
	ia: {
		nom: 'IA',
		main: [],
		combinaison: [],
		detailCombinaison: {}
	},
	detailMainJoueur: "rien",
	detailMainIa: "rien",
	winner: null
}
export const actionTypes = {
	GENERER_CARTES: "on génère le jeu de cartes",
	MELANGER_CARTES: "on mélange cartes",
	DISTRIBUER_CARTES: "on distribue les cartes",
	BRULER_CARTE: "on brule une carte",
	TIRER_LA_TURN: "on tire la turn",
	TIRER_LA_RIVER: "in tire la river",
	SET_DETAIL_MAIN_JOUEUR: "on le détail de la main du joueur",
	SET_DETAIL_MAIN_IA: "on le détail de la main de ia",
	SET_WINNER: "on set le gagnant",
}

export const actionsGameReducer = {
	genererCartes: () => (dispatch) => {
		let arrayCarte = [];
		couleurs.map((couleur) => {
			rangs.map((rang) => {
				let carte = {};
				carte.rang = rang;
				carte.couleur = couleur;
				carte.icon = getIcons(couleur);
				arrayCarte.push(carte);
			});
		});
		initialState.cartes = arrayCarte
		initialState.joueur.main = []
		initialState.joueur.combinaison = ''
		initialState.joueur.detailCombinaison = {}
		initialState.ia.main = []
		initialState.ia.combinaison = ''
		initialState.ia.detailCombinaison = {}
		initialState.cartesBrulees = []
		initialState.cartesTableau = []
		initialState.winner = null
		dispatch({ type: actionTypes.GENERER_CARTES})


	},
	melangerCartes: () => dispatch => {
		const jeuxMelange = shuffle(initialState.cartes);
		let jeuxMelange2 = shuffle(jeuxMelange);
		let jeuxMelange3 = shuffle(jeuxMelange2);
		initialState.cartes = jeuxMelange3
		dispatch({ type: actionTypes.MELANGER_CARTES})
	},
	distribuerCartes: () => dispatch => {
		let jeuxEnCours = initialState.cartes;
		let joueur1 = initialState.joueur
		let IA = initialState.ia
		let combinaisonJoueur = []
		let combinaisonIa = []
		let carte1 = jeuxEnCours.shift()
		joueur1.main.push(carte1)
		combinaisonJoueur.push(carte1)
		let carte2 = jeuxEnCours.shift()
		joueur1.main.push(carte2)
		combinaisonJoueur.push(carte2)
		let carte3 = jeuxEnCours.shift()
		IA.main.push(carte3)
		combinaisonIa.push(carte3)
		let carte4 = jeuxEnCours.shift()
		IA.main.push(carte4)
		combinaisonIa.push(carte4)

		// on brule une carte
		let carteBrule = [jeuxEnCours.shift()]
		// on construit le tableau
		let cartesTableau = []
		let carteTableau1 =  jeuxEnCours.shift()
		cartesTableau.push(carteTableau1)
		combinaisonJoueur.push(carteTableau1)
		combinaisonIa.push(carteTableau1)
		let carteTableau2 =  jeuxEnCours.shift()
		cartesTableau.push(carteTableau2)
		combinaisonJoueur.push(carteTableau2)
		combinaisonIa.push(carteTableau2)
		let carteTableau3 =  jeuxEnCours.shift()
		cartesTableau.push(carteTableau3)
		combinaisonJoueur.push(carteTableau3)
		combinaisonIa.push(carteTableau3)

		initialState.ia = IA
		initialState.joueur = joueur1
		initialState.cartes = jeuxEnCours
		initialState.cartesTableau = cartesTableau
		initialState.cartesBrulees = carteBrule

		console.log('nouveal api: ',  hand.solve(['Ad', 'As', 'Jc', 'Th', '2d']))
		//test détail main des joueurs
		// on le garde dans un state


		console.log('combinaison jour : ', combinaisonJoueur)
		let combinaisonFormated = []
		combinaisonJoueur.map((carte) => {
			combinaisonFormated.push(formatedMain(carte))
		})
		console.log('combinaison formated : ', combinaisonFormated)

		initialState.joueur.detailCombinaison = hand.solve(combinaisonFormated)
		initialState.joueur.combinaison = combinaisonFormated
		let combinaisonIaFormated = []
		combinaisonIa.map((carte) => {
			combinaisonIaFormated.push(formatedMain(carte))
		})
		console.log('combinaison ia formated : ', combinaisonIaFormated)

		initialState.ia.detailCombinaison = hand.solve(combinaisonIaFormated)
		initialState.ia.combinaison = combinaisonIaFormated


		dispatch({ type: actionTypes.DISTRIBUER_CARTES})
	},
	addJoueur: (nom) => dispatch => {
		initialState.joueur = {
			...initialState.joueur,
			nom: nom,
			main: []
		}
	},
	brulerCarte: () => dispatch => {
		let jeuEnCours = initialState.cartes
		jeuEnCours = jeuEnCours.shift()
		initialState.cartes = jeuEnCours
		dispatch({ type: actionTypes.BRULER_CARTE})
	},
	tirerLaTurn: () => dispatch => {
		let jeuEnCours = initialState.cartes
		let carteBrule = jeuEnCours.shift()
		let cartesBrule = initialState.cartesBrulees
		cartesBrule.push(carteBrule);
		let turn = jeuEnCours.shift();
		initialState.cartes = jeuEnCours
		let tableau = initialState.cartesTableau
		tableau.push(turn)

		let joueur = initialState.joueur
		let ia = initialState.ia
		let combinaisonJoueur = joueur.combinaison
		let combinaisonIa = ia.combinaison
		combinaisonJoueur.push(formatedMain(turn));
		combinaisonIa.push(formatedMain(turn));
		console.log('combinaison turn joueur : ', combinaisonJoueur)
		console.log('combinaison turn ia : ', combinaisonIa)
		/*const myPokerHand = new PokerHand(combinaisonJoueur);
		const iaPokerHand = new PokerHand(combinaisonIa);
		console.log('détail main joueur : ', myPokerHand.describe())
		console.log('détail main ia : ', myPokerHand.describe())*/
		joueur.combinaison = combinaisonJoueur
		joueur.detailCombinaison = hand.solve(combinaisonJoueur)
		initialState.joueur = joueur

		ia.combinaison = combinaisonIa
	    ia.detailCombinaison = hand.solve(combinaisonIa)
		initialState.ia = ia

		dispatch({ type: actionTypes.TIRER_LA_TURN})
	},
	tirerLaRiver: () => dispatch => {
		let jeuEnCours = initialState.cartes
		let carteBrule = jeuEnCours.shift()
		let cartesBrule = initialState.cartesBrulees
		cartesBrule.push(carteBrule);
		let tableau = initialState.cartesTableau
		let river = jeuEnCours.shift();
		initialState.cartes = jeuEnCours
		tableau.push(river)

		let joueur = initialState.joueur
		let ia = initialState.ia
		let combinaisonJoueur = joueur.combinaison
		let combinaisonIa = ia.combinaison
		combinaisonJoueur.push(formatedMain(river));
		combinaisonIa.push(formatedMain(river));
		console.log('combinaison river joueur : ', combinaisonJoueur)
		console.log('combinaison river ia : ', combinaisonIa)

		joueur.combinaison = combinaisonJoueur
		joueur.detailCombinaison = hand.solve(combinaisonJoueur)
		initialState.joueur = joueur

		ia.combinaison = combinaisonIa
		ia.detailCombinaison = hand.solve(combinaisonIa)
		initialState.ia = ia

		var hand1 = hand.solve(combinaisonJoueur);
		var hand2 = hand.solve(combinaisonIa);
		var win = hand.winners([hand1, hand2]);
		console.log(win[0].descr)

		let winner = null
		if(joueur.detailCombinaison.descr === win[0].descr) {
			winner = joueur.nom
		}
		if(ia.detailCombinaison.descr === win[0].descr) {
			winner = ia.nom
		}
		initialState.winner = winner

		dispatch({ type: actionTypes.TIRER_LA_RIVER})

	},
	setDetailMainJoueur: (detail) => dispatch => {
		initialState.detailMainJoueur = detail
		dispatch({ type: actionTypes.SET_DETAIL_MAIN_JOUEUR})
	},
	setDetailMainIa: (detail) => dispatch => {
		initialState.detailMainIa = detail
		dispatch({ type: actionTypes.SET_DETAIL_MAIN_IA})
	},
	setWinner: () => dispatch => {
		const joueur = hand.solve(initialState.joueur.combinaison);
		const ia = hand.solve(initialState.ia.combinaison);
		initialState.winner = hand.winners([joueur, ia]); // hand2
		dispatch({ type: actionTypes.SET_WINNER})
	},

}

const GameReducer = (state = initialState, action) => {
	switch (action.type) {
		case actionTypes.GENERER_CARTES:
			return {
				...state,
				cartes: initialState.cartes,
				joueur: initialState.joueur,
				ia: initialState.ia,
				cartesBrulees: initialState.cartesBrulees,
				cartesTableau: initialState.cartesTableau,
				winner: initialState.winner
			}
		case actionTypes.MELANGER_CARTES:
			return { ...state, cartes: initialState.cartes }
		case actionTypes.SET_DETAIL_MAIN_JOUEUR:
			return { ...state, detailMainJoueur: initialState.detailMainJoueur }
		case actionTypes.SET_DETAIL_MAIN_IA:
			return { ...state, detailMainIa: initialState.detailMainIa }
		case actionTypes.BRULER_CARTE:
			return { ...state, cartes: initialState.cartes }
		case actionTypes.SET_WINNER:
			return { ...state, winner: initialState.winner }
		case actionTypes.DISTRIBUER_CARTES:
			return {
				...state,
				joueur: initialState.joueur,
				ia: initialState.ia,
				cartes: initialState.cartes,
				cartesBrulees: initialState.cartesBrulees,
				cartesTableau: initialState.cartesTableau
			}
		case actionTypes.TIRER_LA_TURN:
			return {
				...state,
				cartes: initialState.cartes,
				cartesBrulees: initialState.cartesBrulees,
				cartesTableau: initialState.cartesTableau,
				joueur: initialState.joueur,
				ia: initialState.ia
			}
		case actionTypes.TIRER_LA_RIVER:
			return {
				...state,
				cartes: initialState.cartes,
				cartesBrulees: initialState.cartesBrulees,
				cartesTableau: initialState.cartesTableau,
				joueur: initialState.joueur,
				ia: initialState.ia,
				winner: initialState.winner
			}
		default:
			return state
	}
}

const getIcons = (couleur) => {
	let icon = "";
	switch (couleur.nom) {
		case "pique":
			icon = <GiSpades />;
			break;
		case "coeur":
			icon = <IconContext.Provider value={{ color: "red", className: "global-class-name" }}><GiHearts  /></IconContext.Provider>;
			break;
		case "carreau":
			icon = <IconContext.Provider value={{ color: "red", className: "global-class-name" }}><GiDiamonds /></IconContext.Provider>;
			break;
		case "treffle":
			icon = <GiClubs />;
			break;
		default:
			break;
	}
	return icon;
};

const shuffle = (array) => {
	return array.sort(() => Math.random() - 0.5);
};

const formatedMain = (main) => {
	return main.rang.valeur + main.couleur.lettre
}

export default GameReducer

