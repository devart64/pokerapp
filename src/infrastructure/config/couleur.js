const couleurs = [
	{
		lettre: 'S',
		nom: "pique"
	},
	{
		lettre: 'H',
		nom: "coeur"
	},
	{
		lettre: 'D',
		nom: "carreau"
	},
	{
		lettre: 'C',
		nom: "treffle"
	}
];
export default couleurs;