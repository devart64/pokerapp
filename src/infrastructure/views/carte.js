import React from 'react'
const Carte = (props) => {
    
        return (
            <div className="card border-black m-2"
                style={{
                width: "120px",
                height: "150px",
                display: "inline-block",
                }}
            >
                <div className={"card-header text-start "+ props.carte.couleur}>{props.carte.rang.lettre}</div>
                <div className="card-body">{props.carte.icon}</div>
                <div className={"card-footer text-end "+ props.carte.couleur}>{props.carte.rang.lettre}</div>
            </div>
        )
    
}

export default Carte